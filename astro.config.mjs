import { defineConfig } from 'astro/config';
import mdx from "@astrojs/mdx";

// https://astro.build/config
export default defineConfig({
  sitemap: true,
  site: 'https://epmills.com/',
  outDir: 'public',
  publicDir: 'static',
  integrations: [mdx()]
});